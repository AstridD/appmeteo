const weatherIcons = {
    "Rain": "wi wi-day-rain",
    "Clouds": "wi wi-day-cloudy",
    "Clear": "wi wi-day-sunny",
    "Snow": "wi wi-day-snow",
    "mist": "wi wi-day-fog",
    "Drizzle": "wi wi-day-sleet",
}


//fct pour mettre la 1ere lettre du mot en capitale:
function capitalize(str){
    return str[0].toUpperCase() + str.slice(1);
}


// fct asynchrone av un param par defaut
async function main(withIp = true){
    let ville;
// si on affiche la ville de notre adresse ip, affiche la suite
    if(withIp){

    // recuperer l adresse IP de lordi qui ouvre la page ipify ci dessous:
    //await demande a la fct d executer le code:
    const ip = await fetch(`https://api.ipify.org?format=json`) //fetch est une fct asynchrone, elle lance sa fct puis execute direct le console.log en dehors 
    .then(resultat => resultat.json())
    .then(json => json.ip)
    
 
        // recuperer la ville grace a l adresse IP:
        ville = await fetch(
        `http://api.ipstack.com/${ip}?access_key=0a6a395150135680ee8ff10615eae821`)
        .then(resultat => resultat.json())
        .then(json => json.city)


//sinon il se passera les evnts suivants :
    } else {

        //appel de l id du span de ville:
        ville = document.querySelector('#ville').textContent;
    }

            const meteo = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${ville}&APPID=b512a65df1bd510bed624d57fa30571c&lang=fr&units=metric`)
                .then(resultat => resultat.json())
                .then(json => json)         
     console.log(meteo);
                //afficher les infos sur la page
                displayWeatherInfos(meteo)
}

//afficher les infos meteo
function displayWeatherInfos(data){
    //recuperer les infos du json:
    const name = data.name;
    const temp = data.main.temp;
    const condition = data.weather[0].main;
    const description = data.weather[0].description;

    //affiche le nom de la ville
    document.querySelector('#ville').textContent = name;

    //affiche et arrondie la temperature
    document.querySelector('#temperature').textContent = Math.round(temp);

    //affiche les conditions meteo entre parentheses
    document.querySelector('#conditions').textContent = capitalize(description);

    //affiche l icone du temps en faisant appel à la const plus haut:
    document.querySelector('i.wi').className = weatherIcons[condition];

    // affiche le background/pictures du temps
    document.body.className = condition.toLowerCase();  
}


//creer une fct qui lorsque on clique sur la ville on peut ecrire le nom d une autre
const ville = document.querySelector('#ville');
ville.addEventListener('click', () => {
    ville.contentEditable = true;
});

//lorsq on a ecrit une autre ville, la touche Entree declenche l event
ville.addEventListener('keydown', (e) => {
    if(e.keyCode === 13){ //13 est le keycode de la touche entree
        e.preventDefault(); //stopper l evnt pour ne pas ecrire 15 lignes
        ville.contentEditable = false;
        main(false);
    }
})
main();